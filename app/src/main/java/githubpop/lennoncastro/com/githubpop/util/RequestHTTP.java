package githubpop.lennoncastro.com.githubpop.util;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class RequestHTTP {

    private static RequestHTTP instance;
    private RequestQueue requestQueue;

    private RequestHTTP(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized RequestHTTP getInstance(Context context) {
        if (instance == null) {
            instance = new RequestHTTP(context);
        }

        return instance;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }
}