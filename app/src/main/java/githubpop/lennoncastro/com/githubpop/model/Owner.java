package githubpop.lennoncastro.com.githubpop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mac on 17/02/2018.
 */

public class Owner implements Parcelable {

    private long id;
    private String login;
    private String avatar_url;
    private String url;

    public Owner(){
    }

    protected Owner(Parcel in) {
        id = in.readLong();
        login = in.readString();
        avatar_url = in.readString();
        url = in.readString();
    }

    public static final Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(login);
        dest.writeString(avatar_url);
        dest.writeString(url);
    }
}
