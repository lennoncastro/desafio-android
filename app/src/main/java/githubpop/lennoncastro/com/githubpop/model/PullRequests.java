package githubpop.lennoncastro.com.githubpop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mac on 17/02/2018.
 */

public class PullRequests implements Parcelable {

    private long id;
    private Owner owner;
    private String name;
    private String html_url;
    private String description;
    private String created_at;

    public PullRequests() {}

    public PullRequests(Parcel in) {
        id = in.readLong();
        owner = in.readParcelable(Owner.class.getClassLoader());
        name = in.readString();
        html_url = in.readString();
        description = in.readString();
        created_at = in.readString();
    }

    public static final Creator<PullRequests> CREATOR = new Creator<PullRequests>() {
        @Override
        public PullRequests createFromParcel(Parcel in) {
            return new PullRequests(in);
        }

        @Override
        public PullRequests[] newArray(int size) {
            return new PullRequests[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Creator<PullRequests> getCREATOR() {
        return CREATOR;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeParcelable(owner, flags);
        dest.writeString(html_url);
        dest.writeString(description);
        dest.writeString(name);
        dest.writeString(created_at);
    }
}
