package githubpop.lennoncastro.com.githubpop.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import githubpop.lennoncastro.com.githubpop.R;
import githubpop.lennoncastro.com.githubpop.controller.PullRequestsCTRL;
import githubpop.lennoncastro.com.githubpop.model.PullRequests;
import githubpop.lennoncastro.com.githubpop.model.Repository;
import githubpop.lennoncastro.com.githubpop.view.adapter.PullAdapter;

public class PullRequestsACT extends AppCompatActivity {

    private Repository repository;
    private List<PullRequests> listPullRequests;
    private LinearLayoutManager layoutManager;
    private PullAdapter pullAdapter;
    private PullRequestsCTRL pullCTRL;
    @BindView(R.id.rvListPulls) RecyclerView rvListPulls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request_act);
        ButterKnife.bind(this);
        initialize();

        if(savedInstanceState != null) {
            pullAdapter.clear();
            pullAdapter.add(savedInstanceState.<PullRequests>getParcelableArrayList("listPullRequests"));
        } else {
            loadPage();
        }

    }

    private void initialize() {
        repository        = getIntent().getExtras().getParcelable("repository");

        setTitle(repository.getName());

        listPullRequests  = new ArrayList<>();
        pullAdapter       = new PullAdapter(this, listPullRequests);
        pullCTRL          = new PullRequestsCTRL(this);
        layoutManager     = new LinearLayoutManager(this);

        rvListPulls.setLayoutManager(layoutManager);
        rvListPulls.setAdapter(pullAdapter);
    }

    private void loadPage() {
        pullCTRL.requestPulls(repository);
    }

    public void updatePulls(List<PullRequests> listPullRequests) {
        pullAdapter.add(listPullRequests);
    }

    public void openInBrowser(String url) {
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("listPullRequests", (ArrayList<? extends Parcelable>) listPullRequests);
        outState.putInt("position", rvListPulls.getVerticalScrollbarPosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        pullAdapter.clear();
        pullAdapter.add(savedInstanceState.<PullRequests>getParcelableArrayList("listPullRequests"));
    }

}
