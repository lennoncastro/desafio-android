package githubpop.lennoncastro.com.githubpop.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import githubpop.lennoncastro.com.githubpop.R;
import githubpop.lennoncastro.com.githubpop.controller.RepositoryCTRL;
import githubpop.lennoncastro.com.githubpop.model.Repository;
import githubpop.lennoncastro.com.githubpop.view.adapter.RepositoryAdapter;
import githubpop.lennoncastro.com.githubpop.view.widget.EndlessRecyclerViewScrollListener;

public class MainACT extends AppCompatActivity {

    private List<Repository> listRepositories;
    private RepositoryAdapter repositoryAdapter;
    private RepositoryCTRL repositoryCTRL;
    private int page = 0;

    private LinearLayoutManager layoutManager;
    private EndlessRecyclerViewScrollListener scrollListener;
    @BindView(R.id.rvListRepositories) RecyclerView rvListRepositories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_act);
        ButterKnife.bind(this);
        initialize();

        if(savedInstanceState != null) {
            page = savedInstanceState.getInt("page");
            repositoryAdapter.clear();
            repositoryAdapter.add(savedInstanceState.<Repository>getParcelableArrayList("listRepositories"));
            rvListRepositories.setVerticalScrollbarPosition(savedInstanceState.getInt("position"));
        } else {
            loadPage(page);
        }

    }

    private void initialize() {
        repositoryCTRL    = new RepositoryCTRL(this);
        listRepositories  = new ArrayList<>();
        repositoryAdapter = new RepositoryAdapter(this, listRepositories);
        layoutManager     = new LinearLayoutManager(this);
        scrollListener    = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadPage(page);
            }
        };
        rvListRepositories.addOnScrollListener(scrollListener);
        rvListRepositories.setLayoutManager(layoutManager);
        rvListRepositories.setAdapter(repositoryAdapter);
    }

    private void loadPage(int page) {
        page++;
        repositoryCTRL.requestRepositories(page);
    }

    public void updateRepositories(List<Repository> listRepositories) {
        repositoryAdapter.add(listRepositories);
    }

    public void loadPullRequest(Repository repository) {
        Intent intent = new Intent(this, PullRequestsACT.class);
        intent.putExtra("repository", repository);
        startActivity(intent);
    }

    public void message(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("listRepositories", (ArrayList<? extends Parcelable>) listRepositories);
        outState.putInt("position", rvListRepositories.getVerticalScrollbarPosition());
        outState.putInt("page", page);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        page = savedInstanceState.getInt("page");
        repositoryAdapter.clear();
        repositoryAdapter.add(savedInstanceState.<Repository>getParcelableArrayList("listRepositories"));
        rvListRepositories.setVerticalScrollbarPosition(savedInstanceState.getInt("position"));
    }


}
