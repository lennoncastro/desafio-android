package githubpop.lennoncastro.com.githubpop.view.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import githubpop.lennoncastro.com.githubpop.R;
import githubpop.lennoncastro.com.githubpop.model.PullRequests;
import githubpop.lennoncastro.com.githubpop.view.activity.PullRequestsACT;

/**
 * Created by mac on 17/02/2018.
 */

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.Holder> {

    private Context context;
    private List<PullRequests> pullList;

    public PullAdapter(Context context, List<PullRequests> pullList) {
        this.context        = context;
        this.pullList       = pullList;
    }

    @Override
    public void onBindViewHolder(PullAdapter.Holder holder, int position) {
        final PullRequests pullRequests = pullList.get(position);

        holder.cvPull.setTag(pullRequests.getId());
        holder.tvLogin.setText(pullRequests.getOwner().getLogin());
        holder.tvName.setText(pullRequests.getName());
        holder.tvDescription.setText(pullRequests.getDescription());
        holder.tvCreatedAt.setText(pullRequests.getCreated_at());
        Glide.with(context).load(pullRequests.getOwner().getAvatar_url()).fitCenter().into(holder.ivAvatar_url);

        holder.cvPull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PullRequestsACT) context).openInBrowser(pullRequests.getHtml_url());
            }
        });

    }

    public void add(List<PullRequests> pullList) {
        for(int i = 0; i < pullList.size(); i++) {
            this.pullList.add(pullList.get(i));
        }
        this.notifyDataSetChanged();
    }

    public void clear() {
        this.pullList.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return pullList.size();
    }

    @Override
    public PullAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pull_layout, parent, false);

        return new PullAdapter.Holder(itemView);
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvLogin) TextView tvLogin;
        @BindView(R.id.cvPull) CardView cvPull;

        @BindView(R.id.tvName) TextView tvName;
        @BindView(R.id.tvDescription) TextView tvDescription;
        @BindView(R.id.tvCreatedAt) TextView tvCreatedAt;

        @BindView(R.id.ivAvatar_url) ImageView ivAvatar_url;

        public Holder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

