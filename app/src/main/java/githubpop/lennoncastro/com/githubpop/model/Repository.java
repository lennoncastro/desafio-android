package githubpop.lennoncastro.com.githubpop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mac on 17/02/2018.
 */

public class Repository implements Parcelable {

    private long id;
    private long stargazers_count;
    private long forks_count;
    private String name;
    private String full_name;
    private String description;
    private String html_url;
    private String forks_url;
    private Owner owner;

    public Repository() {}

    public Repository(Parcel in) {
        id = in.readLong();
        stargazers_count = in.readLong();
        forks_count = in.readLong();
        name = in.readString();
        full_name = in.readString();
        description = in.readString();
        html_url = in.readString();
        forks_url = in.readString();
        owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public long getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(long stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getForks_url() {
        return forks_url;
    }

    public void setForks_url(String forks_url) {
        this.forks_url = forks_url;
    }

    public long getForks_count() {
        return forks_count;
    }

    public void setForks_count(long forks_count) {
        this.forks_count = forks_count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(stargazers_count);
        dest.writeLong(forks_count);
        dest.writeString(name);
        dest.writeString(full_name);
        dest.writeString(description);
        dest.writeString(html_url);
        dest.writeString(forks_url);
        dest.writeParcelable(owner, flags);
    }
}
