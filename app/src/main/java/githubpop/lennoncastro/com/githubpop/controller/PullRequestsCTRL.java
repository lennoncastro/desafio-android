package githubpop.lennoncastro.com.githubpop.controller;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import githubpop.lennoncastro.com.githubpop.model.Owner;
import githubpop.lennoncastro.com.githubpop.model.PullRequests;
import githubpop.lennoncastro.com.githubpop.model.Repository;
import githubpop.lennoncastro.com.githubpop.util.Data;
import githubpop.lennoncastro.com.githubpop.util.InternetConection;
import githubpop.lennoncastro.com.githubpop.util.RequestHTTP;
import githubpop.lennoncastro.com.githubpop.view.activity.PullRequestsACT;

/**
 * Created by mac on 17/02/2018.
 */

public class PullRequestsCTRL {

    private Context context;
    private RequestQueue requestQueue;
    private String url;
    private List<PullRequests> listPullRequests;

    public PullRequestsCTRL(Context context) {
        this.context = context;
        requestQueue = RequestHTTP.getInstance(context).getRequestQueue();
    }

    public void requestPulls(Repository repository) {

        url             = repository.getForks_url();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        addListPullRequests(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((PullRequestsACT) context).message("Error when sending request.");
                    }
                });

        if(InternetConection.hasConnection(context))
            requestQueue.add(stringRequest);
        else
            ((PullRequestsACT) context).message("No internet conection.");

    }

    private List<PullRequests> addListPullRequests(String response) {

        listPullRequests = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(response.toString());

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsPull  = jsonArray.getJSONObject(i);
                JSONObject jsOwner = jsPull.getJSONObject("owner");

                Owner owner = new Owner();
                owner.setId(jsOwner.getLong("id"));
                owner.setUrl(jsOwner.getString("url"));
                owner.setAvatar_url(jsOwner.getString("avatar_url"));
                owner.setLogin(jsOwner.getString("login"));

                PullRequests pullRequests = new PullRequests();
                pullRequests.setId(jsPull.getLong("id"));
                pullRequests.setName(jsPull.getString("name"));
                pullRequests.setDescription(jsPull.getString("description"));
                pullRequests.setHtml_url(jsPull.getString("html_url"));
                pullRequests.setCreated_at(new Data().formataDataBR(jsPull.getString("created_at")));
                pullRequests.setOwner(owner);

                listPullRequests.add(pullRequests);
            }

            updatePullRequests();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listPullRequests;
    }

    private void updatePullRequests() {
        ((PullRequestsACT) context).updatePulls(listPullRequests);
    }

}
