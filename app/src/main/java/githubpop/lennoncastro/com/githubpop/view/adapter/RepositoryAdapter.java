package githubpop.lennoncastro.com.githubpop.view.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import githubpop.lennoncastro.com.githubpop.R;
import githubpop.lennoncastro.com.githubpop.model.Repository;
import githubpop.lennoncastro.com.githubpop.view.activity.MainACT;

/**
 * Created by mac on 17/02/2018.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.Holder> {

    private Context context;
    private List<Repository> repositoryList;

    public RepositoryAdapter(Context context, List<Repository> repositoryList) {
        this.context        = context;
        this.repositoryList = repositoryList;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        final Repository repository = repositoryList.get(position);

        holder.cvRepository.setTag(repository.getHtml_url());
        holder.tvLogin.setText(repository.getOwner().getLogin());
        holder.tvName.setText(repository.getName());
        holder.tvDescription.setText(repository.getDescription());
        holder.tvStargazersCount.setText(String.valueOf(repository.getStargazers_count()));
        holder.tvForksCount.setText(String.valueOf(repository.getForks_count()));

        if(!repository.getOwner().getAvatar_url().equals(""))
            Glide.with(context).load(repository.getOwner().getAvatar_url()).fitCenter().into(holder.ivAvatar_url);

        holder.cvRepository.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainACT)context).loadPullRequest(repository);
            }
        });

    }

    public void add(List<Repository> rList) {
        for(int i = 0; i < rList.size(); i++) {
            this.repositoryList.add(rList.get(i));
        }
        this.notifyDataSetChanged();
    }

    public void clear() {
        this.repositoryList.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return repositoryList.size();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repository_layout, parent, false);

        return new Holder(itemView);
    }


    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.cvRepository) CardView cvRepository;

        @BindView(R.id.tvName) TextView tvName;
        @BindView(R.id.tvLogin) TextView tvLogin;
        @BindView(R.id.tvDescription) TextView tvDescription;
        @BindView(R.id.tvStargazersCount) TextView tvStargazersCount;
        @BindView(R.id.tvForksCount) TextView tvForksCount;

        @BindView(R.id.ivAvatar_url) ImageView ivAvatar_url;
        @BindView(R.id.ivStargazersCount) ImageView ivStargazersCount;
        @BindView(R.id.ivForksCount) ImageView ivForksCount;

        public Holder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
