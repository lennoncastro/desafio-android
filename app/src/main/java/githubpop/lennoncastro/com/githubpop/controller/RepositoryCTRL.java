package githubpop.lennoncastro.com.githubpop.controller;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import githubpop.lennoncastro.com.githubpop.R;
import githubpop.lennoncastro.com.githubpop.model.Owner;
import githubpop.lennoncastro.com.githubpop.model.Repository;
import githubpop.lennoncastro.com.githubpop.util.InternetConection;
import githubpop.lennoncastro.com.githubpop.util.RequestHTTP;
import githubpop.lennoncastro.com.githubpop.view.activity.MainACT;

public class RepositoryCTRL {

    private Context context;
    private RequestQueue requestQueue;
    private List<Repository> listRepositories;
    private String url;

    public RepositoryCTRL(Context context) {
        this.context     = context;
        requestQueue     = RequestHTTP.getInstance(context).getRequestQueue();
    }

    public void requestRepositories(int page) {

        url              = context.getResources().getString(R.string.endpoint)
                         + "search/repositories?q=language:Java&sort=stars&page=" + page;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                            addListRepositories(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((MainACT) context).message("Error when sending request.");
                    }
        });

        if(InternetConection.hasConnection(context))
            requestQueue.add(jsonObjectRequest);
        else
            ((MainACT) context).message("No internet conection.");

    }

    private void addListRepositories(JSONObject jsonObject) {

        listRepositories = new ArrayList<>();

        try {

            JSONArray items = jsonObject.getJSONArray("items");

            for(int i = 0; i < items.length(); i++) {
                JSONObject item     = items.getJSONObject(i);
                JSONObject jsOwner  = item.getJSONObject("owner");

                Owner owner = new Owner();
                owner.setId(jsOwner.getLong("id"));
                owner.setLogin(jsOwner.getString("login"));
                owner.setUrl(jsOwner.getString("url"));
                owner.setAvatar_url(jsOwner.getString("avatar_url"));

                Repository repository = new Repository();
                repository.setName(item.getString("name"));
                repository.setDescription(item.getString("description"));
                repository.setStargazers_count(item.getLong("stargazers_count"));
                repository.setForks_count(item.getLong("forks"));
                repository.setForks_url(item.getString("forks_url"));
                repository.setHtml_url(item.getString("html_url"));
                repository.setOwner(owner);

                listRepositories.add(repository);
            }

            updateRepositories();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateRepositories() {
        ((MainACT) context).updateRepositories(listRepositories);
    }

}
