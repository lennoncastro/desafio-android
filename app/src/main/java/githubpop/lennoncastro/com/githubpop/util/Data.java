package githubpop.lennoncastro.com.githubpop.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Data {

    public Data() {}

    public static String formataDataBR(String data) throws Exception{

        String dataFormatada = "";

        try {
            DateFormat inputFormatter1  = new SimpleDateFormat("yyyy-MM-dd");
            Date date1                  = inputFormatter1.parse(data);

            DateFormat outputFormatter1 = new SimpleDateFormat("dd/MM/yyyy");
            dataFormatada              = outputFormatter1.format(date1); //

        } catch (Exception e) {}

        return dataFormatada;

    }


}
